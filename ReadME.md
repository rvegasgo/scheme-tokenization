#Project AIM 
The project aims to run a batch update in DB by running a jar for 2 use cases
    Generate Credit Card PIT
    Generate Scheme Token PIT

#How to run
To run for Credit Card PIT </br>
    
    SET SchemeTokenization.creditCardPIT = true; </br>

To run for Scheme Token PIT </br>
    
    SET SchemeTokenization.schemeTokenPIT = true; </br>

### Extra Information
####Files for "Generate Credit Card PIT"
    Input Data File : credit_card_pci_input.txt
    Output File : output.scv</br>

####Files for "Generate Scheme Token PIT"</br>
    Input Data File : scheme_token_pci_input.txt
    Output File : output.scv


