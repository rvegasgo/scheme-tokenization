package com.edreamsodigeo.worldpay.tokenization.database;

import com.edreamsodigeo.worldpay.tokenization.creditcard.CreditCard;
import com.edreamsodigeo.worldpay.tokenization.information.WorldPayInformation;
import com.edreamsodigeo.worldpay.tokenization.pit.PaymentInstrumentServiceController;
import com.edreamsodigeo.worldpay.tokenization.pit.request.SchemeTokenPciScope;
import com.edreamsodigeo.worldpay.tokenization.pit.response.PaymentInstrumentToken;
import com.edreamsodigeo.worldpay.tokenization.properties.PropertiesInTheSystem;
import com.google.common.base.Strings;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;
import java.util.Optional;

public class Oracle {

    private static final Logger LOGGER = Logger.getLogger(Oracle.class);

    private static final PropertiesInTheSystem PROPERTIES_IN_THE_SYSTEM = PropertiesInTheSystem.getInstance();
    private static final PaymentInstrumentServiceController PAYMENT_INSTRUMENT_SERVICE_CONTROLLER = PaymentInstrumentServiceController.getInstance();
    private static final String HOST = "host";
    private static final String PORT = "port";
    private static final String SERVICE_NAME = "service_name";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String GET_CREDIT_CARD_BY_ID_QUERY = "SELECT CARD_NUMBER, OWNER, EXPIRATION_DATE FROM LAUNCH.ED_CREDIT_CARDS WHERE ID = ?";
    private static final String INFORM_PIT_BY_RECURRING_COLLECTION = "UPDATE LAUNCH.ED_SALES_ITEMS SET PAYMENT_INSTRUMENT_TOKEN = ? WHERE RECURRING_COLLECTION_ID = ?";

    private static Connection connection;

    public static void createConnection() throws SQLException {
        connection = DriverManager.getConnection(
                "jdbc:oracle:thin:@"+PROPERTIES_IN_THE_SYSTEM.getProperty(HOST)+":"+PROPERTIES_IN_THE_SYSTEM.getProperty(PORT)+"/"+PROPERTIES_IN_THE_SYSTEM.getProperty(SERVICE_NAME), PROPERTIES_IN_THE_SYSTEM.getProperty(USERNAME), PROPERTIES_IN_THE_SYSTEM.getProperty(PASSWORD));
    }

    public static Optional<CreditCard> getCreditCardByCollectionEntityId(String collectionEntityId, String creditCardUriPath) {
        CreditCard creditCard = null;
        try (Statement stmt = connection.createStatement()) {
            try (PreparedStatement sentencia = connection.prepareStatement(GET_CREDIT_CARD_BY_ID_QUERY)) {
                sentencia.setString(1, collectionEntityId);
                try (ResultSet resultSet = sentencia.executeQuery()) {
                    if (resultSet.next()) {
                        String number = resultSet.getString("CARD_NUMBER");
                        String holder = resultSet.getString("OWNER");
                        String expirationDate = resultSet.getString("EXPIRATION_DATE");
                        String expirationYear = null;
                        String expirationMonth = null;
                        if (Objects.nonNull(expirationDate)) {
                            expirationDate = Strings.padStart(expirationDate, 4, '0');
                            expirationMonth = expirationDate.substring(0, 2);
                            expirationYear = expirationDate.substring(2, 4);
                        }
                        creditCard = new CreditCard(number, holder, expirationMonth, expirationYear);
                        PAYMENT_INSTRUMENT_SERVICE_CONTROLLER.getCreditCardPIT(collectionEntityId, creditCardUriPath, creditCard)
                                .map(PaymentInstrumentToken::toString)
                                .ifPresent(creditCard::setPaymentInstrumentToken);
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return Optional.ofNullable(creditCard);
    }

    public static int informPITByRecurringCollection(String recurringCollectionId, String paymentInstrumentToken) {

        try (Statement stmt = connection.createStatement()) {
            try (PreparedStatement sentencia = connection.prepareStatement(INFORM_PIT_BY_RECURRING_COLLECTION)) {
                sentencia.setString(1, paymentInstrumentToken);
                sentencia.setString(2, recurringCollectionId);
                return sentencia.executeUpdate();
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return -1;
    }

    public static Optional<PaymentInstrumentToken> getPCIScopeSchemeToken(WorldPayInformation token, String schemeTokenUriPath) {
        String worldPayTokenExpiry = token.getWorldPayTokenExpiry();
        if (Objects.nonNull(token.getWorldPayToken()) &&
                Objects.nonNull(worldPayTokenExpiry)) {
            String expirationMonth = worldPayTokenExpiry.substring(0, 2);
            String expirationYear = "20" + worldPayTokenExpiry.substring(2, 4);
            SchemeTokenPciScope pciScope = new SchemeTokenPciScope(token.getWorldPayToken(), expirationMonth, expirationYear);
            return PAYMENT_INSTRUMENT_SERVICE_CONTROLLER.getSchemeTokenPIT(pciScope, schemeTokenUriPath);
        }
        return Optional.empty();
    }

    public static void closeConnection() throws SQLException {
        if (Objects.nonNull(connection)) {
            connection.close();
        }
    }

}
