package com.edreamsodigeo.worldpay.tokenization.creditcard;

import java.util.Objects;
import java.util.Optional;

public class CreditCard {

    private String number;
    private String holder;
    private String expirationMonth;
    private String expirationYear;
    private String cvv;
    private String paymentInstrumentToken;

    public CreditCard() {
    }

    public CreditCard(String number, String holder, String expirationMonth, String expirationYear) {
        this.number = number;
        this.holder = holder;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getPaymentInstrumentToken() {
        return paymentInstrumentToken;
    }

    public void setPaymentInstrumentToken(String paymentInstrumentToken) {
        this.paymentInstrumentToken = paymentInstrumentToken;
    }

    @Override
    public String toString() {
        String last4Digits = Optional.ofNullable(number)
                .filter(creditCardNumber -> Objects.nonNull(creditCardNumber) && creditCardNumber.length() >= 4)
                .map(creditCardNumber -> creditCardNumber.substring(creditCardNumber.length()-4))
                .orElse(null);

        String first6Digits = Optional.ofNullable(number)
                .filter(creditCardNumber -> Objects.nonNull(creditCardNumber) && creditCardNumber.length() >= 6)
                .map(creditCardNumber -> creditCardNumber.substring(0, 6))
                .orElse(null);

        return "CreditCardNumber{" +
                "first6Digits='" + first6Digits + '\'' +
                ", last4Digits='" + last4Digits + '\'' +
                ", paymentInstrumentToken='" + paymentInstrumentToken + '\'' +
                '}';
    }
}
