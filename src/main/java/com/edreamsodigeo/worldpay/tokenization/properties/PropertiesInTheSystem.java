package com.edreamsodigeo.worldpay.tokenization.properties;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class PropertiesInTheSystem extends Properties {

    private static final Logger LOGGER = Logger.getLogger(PropertiesInTheSystem.class);

    private static PropertiesInTheSystem instance;

    private PropertiesInTheSystem() {
        super();
    }

    public static PropertiesInTheSystem getInstance() {
        try {
            if (Objects.isNull(instance)) {
                instance = new PropertiesInTheSystem();
                instance.load(new FileInputStream(new File("properties.properties")));
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return instance;
    }

}
