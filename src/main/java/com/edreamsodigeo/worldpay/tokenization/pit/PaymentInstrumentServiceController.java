package com.edreamsodigeo.worldpay.tokenization.pit;

import com.edreamsodigeo.worldpay.tokenization.creditcard.CreditCard;
import com.edreamsodigeo.worldpay.tokenization.pit.request.SchemeTokenPciScope;
import com.edreamsodigeo.worldpay.tokenization.pit.response.PaymentInstrumentToken;
import com.edreamsodigeo.worldpay.tokenization.properties.PropertiesInTheSystem;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;
import java.util.Optional;

public class PaymentInstrumentServiceController {

    private static final Logger LOGGER = Logger.getLogger(PaymentInstrumentServiceController.class);

    private static final PropertiesInTheSystem PROPERTIES_IN_THE_SYSTEM = PropertiesInTheSystem.getInstance();
    private static final String PAYMENT_INSTRUMENT_URL = "payment_instrument_url";
    private static final String POST = "POST";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json;charset=UTF-8";

    private static PaymentInstrumentServiceController instance;

    public Optional<PaymentInstrumentToken> getCreditCardPIT(String collectionEntityId, String creditCardUriPath, CreditCard creditCard) {
        try {
            URL url = new URL(PROPERTIES_IN_THE_SYSTEM.getProperty(PAYMENT_INSTRUMENT_URL) + creditCardUriPath);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod(POST);
            conn.setRequestProperty(CONTENT_TYPE, CONTENT_TYPE_VALUE);

            Gson gson = new Gson();
            String stringJson = gson.toJson(creditCard);

            OutputStream os = conn.getOutputStream();
            os.write(stringJson.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            PaymentInstrumentToken response = null;
            while ((output = br.readLine()) != null) {
                response = gson.fromJson(output, PaymentInstrumentToken.class);
            }

            conn.disconnect();

            return Optional.ofNullable(response);
        } catch (Exception e) {
            LOGGER.error("Error get PIT from " + collectionEntityId + "." + e.getMessage(), e);
        }
        return Optional.empty();
    }

    public static PaymentInstrumentServiceController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new PaymentInstrumentServiceController();
        }
        return instance;
    }

    public Optional<PaymentInstrumentToken> getSchemeTokenPIT(SchemeTokenPciScope pciScope, String schemeTokenURIPath) {
        try {
            URL url = new URL(PROPERTIES_IN_THE_SYSTEM.getProperty(PAYMENT_INSTRUMENT_URL) + schemeTokenURIPath);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod(POST);
            conn.setRequestProperty(CONTENT_TYPE, CONTENT_TYPE_VALUE);

            Gson gson = new Gson();
            String stringJson = gson.toJson(pciScope);

            OutputStream os = conn.getOutputStream();
            os.write(stringJson.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            PaymentInstrumentToken response = null;
            while ((output = br.readLine()) != null) {
                response = gson.fromJson(output, PaymentInstrumentToken.class);
            }

            conn.disconnect();

            return Optional.ofNullable(response);
        } catch (Exception e) {
            LOGGER.error("Error in getting Scheme token PIT for WP token " + pciScope.getTokenNumber() + "\n" + e.getMessage(), e);
        }
        return Optional.empty();
    }

}
