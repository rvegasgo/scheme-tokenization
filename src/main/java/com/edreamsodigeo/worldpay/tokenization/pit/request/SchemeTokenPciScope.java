package com.edreamsodigeo.worldpay.tokenization.pit.request;

public class SchemeTokenPciScope {
    private String tokenNumber;
    private String tokenExpirationMonth;
    private String tokenExpirationYear;
    private String first6CardNumberDigits;
    private String last4CardNumberDigits;
    private String cardExpirationMonth;
    private String cardExpirationYear;
    private String paymentAccountReference;

    public SchemeTokenPciScope(String tokenNumber, String tokenExpirationMonth, String tokenExpirationYear) {
        this.tokenNumber = tokenNumber;
        this.tokenExpirationMonth = tokenExpirationMonth;
        this.tokenExpirationYear = tokenExpirationYear;
    }

    public String getTokenNumber() {
        return tokenNumber;
    }

    public String getTokenExpirationMonth() {
        return tokenExpirationMonth;
    }

    public String getTokenExpirationYear() {
        return tokenExpirationYear;
    }

}
