package com.edreamsodigeo.worldpay.tokenization.information;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;

public class CreditCardInformation {

    private final String collectionEntityId;
    private final String recurringCollectionId;

    public CreditCardInformation(String collectionEntityId, String recurringCollectionId) {
        this.collectionEntityId = collectionEntityId;
        this.recurringCollectionId = recurringCollectionId;
    }

    public String getCollectionEntityId() {
        return collectionEntityId;
    }

    public String getRecurringCollectionId() {
        return recurringCollectionId;
    }

    public static Map<String, String> readCreditCardPITFile(String creditCardInfoFileName) throws FileNotFoundException {
        Map<String, String> creditCardPITMap = new HashMap<>();
        InputStream ins = new FileInputStream(creditCardInfoFileName);
        Scanner obj = new Scanner(ins);
        Set<CreditCardInformation> data = new HashSet<>();
        while (obj.hasNextLine()) {
            String line = obj.nextLine();
            String [] lines = line.split(";");
            creditCardPITMap.put(lines[0].trim(), lines[1].trim());
        }
        return creditCardPITMap;
    }

    public static Set<CreditCardInformation> readInternalFileWithCreditCardInfo(String ccPciInputFileName) throws FileNotFoundException {
        InputStream ins = new FileInputStream(ccPciInputFileName);
        Scanner obj = new Scanner(ins);
        Set<CreditCardInformation> data = new HashSet<>();
        while (obj.hasNextLine()) {
            String line = obj.nextLine();
            String [] lines = line.split(";");
            data.add(new CreditCardInformation(lines[4].replace("\"", ""), lines[5].replace("\"", "")));
        }
        return data;
    }

    @Override
    public String toString() {
        return "Data{" +
                "collectionEntityId='" + collectionEntityId + '\'' +
                ", recurringCollectionId='" + recurringCollectionId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditCardInformation that = (CreditCardInformation) o;
        return Objects.equals(collectionEntityId, that.collectionEntityId) &&
                Objects.equals(recurringCollectionId, that.recurringCollectionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(collectionEntityId, recurringCollectionId);
    }
}
