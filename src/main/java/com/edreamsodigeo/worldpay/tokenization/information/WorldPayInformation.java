package com.edreamsodigeo.worldpay.tokenization.information;

import com.edreamsodigeo.worldpay.tokenization.database.Oracle;
import com.edreamsodigeo.worldpay.tokenization.pit.response.PaymentInstrumentToken;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

public class WorldPayInformation {
    private static final Logger LOGGER = Logger.getLogger(WorldPayInformation.class);
    private static final String SCHEME_TOKEN = "SCHEME_TOKEN-";
    private static final String DELIMITER = ";";
    private static final String SQL_INSERT_STMNT = "INSERT INTO CREDIT_CARD_SCHEME_TOKEN_MAP (CREDIT_CARD_PIT, SCHEME_TOKEN_PIT) SELECT '%s','%s' FROM dual WHERE NOT EXISTS (SELECT * FROM CREDIT_CARD_SCHEME_TOKEN_MAP WHERE CREDIT_CARD_SCHEME_TOKEN_MAP.CREDIT_CARD_PIT = '%s');";
    private String worldPayToken;
    private String worldPayTokenExpiry;
    private String creditCardId;

    public WorldPayInformation(String worldPayToken, String worldPayTokenExpiry, String creditCardId) {
        this.worldPayToken = worldPayToken;
        this.worldPayTokenExpiry = worldPayTokenExpiry;
        this.creditCardId = creditCardId;
    }

    public static Set<WorldPayInformation> readWorldPayFileExternal(String pciInfoFileName) throws IOException {
        InputStream ins = new FileInputStream(pciInfoFileName);
        Scanner obj = new Scanner(ins);
        Set<WorldPayInformation> data = new HashSet<>();
        while (obj.hasNextLine()) {
            String line = obj.nextLine();
            String [] lines = line.split(";");
            data.add(new WorldPayInformation(lines[0].trim(), lines[1].trim(), lines[2].trim()));
        }
        ins.close();
        return data;
    }

    public static void invokePciScopeSchemeToken(WorldPayInformation worldPayInformation, String schemeTokenUriPath, Map<String, String> creditCardPITMap, FileWriter report) throws IOException {
        Optional<PaymentInstrumentToken> paymentInstrumentTokenOptional = Oracle.getPCIScopeSchemeToken(worldPayInformation, schemeTokenUriPath);
        if (paymentInstrumentTokenOptional.isPresent()) {
            LOGGER.info("PIT informed for Worldpay token with CreditCard ID: " + worldPayInformation.getCreditCardId());
            if(creditCardPITMap.containsKey(worldPayInformation.getCreditCardId())) {
                LOGGER.info("Matching Credit Card PIT informed with CreditCard ID: " + worldPayInformation.getCreditCardId());
                String creditCardPIT = creditCardPITMap.get(worldPayInformation.getCreditCardId());
                String schemeTokenPIT = SCHEME_TOKEN + paymentInstrumentTokenOptional.get().getUuid();
                report.write(creditCardPIT + DELIMITER +
                        schemeTokenPIT + DELIMITER +
                        String.format(SQL_INSERT_STMNT, creditCardPIT,
                                schemeTokenPIT,
                                creditCardPIT) + System.lineSeparator());
            } else {
                LOGGER.error("[NO MATCH] : There is no matching credit card pit for CreditCard ID: " + worldPayInformation.getCreditCardId());
            }
        } else {
            LOGGER.error("No SchemeToken PIT found for WorldPayToken : " + worldPayInformation);
        }
    }

    public String getWorldPayToken() {
        return worldPayToken;
    }

    public String getWorldPayTokenExpiry() {
        return worldPayTokenExpiry;
    }

    public String getCreditCardId() {
        return creditCardId;
    }

    @Override
    public String toString() {
        return "WorldPayToken{" +
                "worldPayToken='" + worldPayToken + '\'' +
                ", worldPayTokenExpiry='" + worldPayTokenExpiry + '\'' +
                ", creditCardId='" + creditCardId + '\'' +
                '}';
    }

}
