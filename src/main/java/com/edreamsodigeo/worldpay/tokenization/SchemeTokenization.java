package com.edreamsodigeo.worldpay.tokenization;

import com.edreamsodigeo.worldpay.tokenization.creditcard.CreditCard;
import com.edreamsodigeo.worldpay.tokenization.database.Oracle;
import com.edreamsodigeo.worldpay.tokenization.information.CreditCardInformation;
import com.edreamsodigeo.worldpay.tokenization.information.WorldPayInformation;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class SchemeTokenization {
    private static final boolean creditCardPIT = false;
    private static final boolean schemeTokenPIT = true;

    private static final Logger LOGGER = Logger.getLogger(SchemeTokenization.class);

    private static final String CREDIT_CARD_INPUT_FILE_NAME = "credit_card_pci_input.txt";
    private static final String CREDIT_CARD_URI_PATH = "pci-scope-credit-cards";

    private static final String CREDIT_CARD_INFO_FILE_NAME = "01_credit_card_info.csv";
    private static final String SCHEME_TOKEN_INFO_FILE_NAME = "02_scheme_token_info.csv";
    private static final String SCHEME_TOKEN_URI_PATH = "pci-scope-scheme-tokens";

    private static final String CREDIT_CARD_OUTPUT_FILE = "credit_card_results.csv";
    private static final String SCHEME_TOKEN_OUTPUT_FILE = "JIRA_DATAEXP_507.csv";

    public static void main(String[] args) throws IOException, SQLException {
        Oracle.createConnection();
        executeBatchProcessToFetchPIT();
        Oracle.closeConnection();
    }

    private static void executeBatchProcessToFetchPIT() throws IOException {
        if(creditCardPIT) {
            FileWriter report = new FileWriter(CREDIT_CARD_OUTPUT_FILE);
            Set<CreditCardInformation> creditCardInformation = CreditCardInformation.readInternalFileWithCreditCardInfo(CREDIT_CARD_INPUT_FILE_NAME);
            for (CreditCardInformation information: creditCardInformation) {
                invokePciScopeCreditCard(information, CREDIT_CARD_URI_PATH, report);
            }
            report.close();
        }
        if(schemeTokenPIT) {
            File creditCardFile = new File(CREDIT_CARD_INFO_FILE_NAME);
            File worldPayFile = new File(SCHEME_TOKEN_INFO_FILE_NAME);
            if(creditCardFile.exists() && worldPayFile.exists()) {
                FileWriter report = new FileWriter(SCHEME_TOKEN_OUTPUT_FILE);
                Set<WorldPayInformation> worldPayInformationSet = WorldPayInformation.readWorldPayFileExternal(SCHEME_TOKEN_INFO_FILE_NAME);
                Map<String, String> creditCardPITMap = CreditCardInformation.readCreditCardPITFile(CREDIT_CARD_INFO_FILE_NAME);
                for (WorldPayInformation token : worldPayInformationSet) {
                    WorldPayInformation.invokePciScopeSchemeToken(token, SCHEME_TOKEN_URI_PATH, creditCardPITMap, report);
                }
                report.close();
            } else {
                LOGGER.error("Please make sure you have input files in the root folder.\n  1. " + CREDIT_CARD_INFO_FILE_NAME + "\n 2. " + SCHEME_TOKEN_INFO_FILE_NAME);
            }
        }
    }

    private static void invokePciScopeCreditCard(CreditCardInformation information, String creditCardUriPath, FileWriter report) throws IOException {
        Optional<CreditCard> creditCardNumberOptional = Oracle.getCreditCardByCollectionEntityId(information.getCollectionEntityId(), creditCardUriPath);
        if (creditCardNumberOptional.isPresent()) {
            CreditCard creditCard = creditCardNumberOptional.get();
            if (Objects.nonNull(creditCard.getPaymentInstrumentToken())) {
                int recordsUpdated = Oracle.informPITByRecurringCollection(information.getRecurringCollectionId(), creditCard.getPaymentInstrumentToken());
                if (recordsUpdated < 0) {
                    LOGGER.error("Occurs a problem when tried to inform pit to " + information.getCollectionEntityId());
                } else if (recordsUpdated == 0) {
                    LOGGER.error("Non register to update " + information.getCollectionEntityId());
                } else {
                    LOGGER.info("PIT informed for " + information + ". Records updated = " + recordsUpdated);
                }
                report.write(information.getCollectionEntityId() + ";" + creditCard.getPaymentInstrumentToken() + ";\n");
            } else {
                LOGGER.error("No matching pit was found for collection entity id: " + information.getCollectionEntityId());
            }
        } else {
            LOGGER.error("No creditCard was found for collection entity id " + information.getCollectionEntityId());
        }
    }

}
